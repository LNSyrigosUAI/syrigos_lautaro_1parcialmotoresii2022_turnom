﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangoDelEnemigo : MonoBehaviour
{
    public Animator animator;
    public Enemigo enemigo;//Referencia del script enemigo.

    void Start()
    {
        
    }

    void Update()
    {
        
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Player"))
        {
            animator.SetBool("Walk", false);
            animator.SetBool("Run", false);
            animator.SetBool("Atacar", true);
            enemigo.Atacar = true;
            GetComponent<CapsuleCollider>().enabled = false;
        }
    }
}
