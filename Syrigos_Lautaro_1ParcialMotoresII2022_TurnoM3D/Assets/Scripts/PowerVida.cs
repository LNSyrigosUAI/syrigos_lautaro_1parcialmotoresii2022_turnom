﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerVida : MonoBehaviour
{
    ControlJugador controlJugador;//Para acceder a la vida del jugador.
    public int cantidad;//Cantidad de vida que quite o regenere.
    public float tiempoDaño;//Tiempo en lo que pasa la accion.
    float tiempoDañoActual;//Lleva el conteo de la accion.

    void Start()
    {
        controlJugador = GameObject.FindWithTag("Player").GetComponent<ControlJugador>(); //Llama al jugador.
    }
    private void OnTriggerStay(Collider other)//Detecta si el jugador entro en contacto con el Power Up.
    {
        if (other.tag == "Player")
        {
            tiempoDañoActual += Time.deltaTime;
            if (tiempoDañoActual > tiempoDaño)//Si el tiempo actual supera al tiempo, va a sumar la cantidad de vida.
            {
                controlJugador.Vida += cantidad;
                tiempoDañoActual = 0.0f;//En caso de usarse para el daño resetea la vida a 0.
            }
        }
    }
}
