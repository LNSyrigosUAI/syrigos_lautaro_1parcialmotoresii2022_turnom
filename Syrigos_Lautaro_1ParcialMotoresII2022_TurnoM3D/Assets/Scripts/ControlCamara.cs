﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCamara : MonoBehaviour
{
    public GameObject player;

    private Vector3 offset;

    void Start()
    {
        //Calcula y almacena el valor de offset obteniendo la distancia entre la posición del jugador y la posición de la cámara.
        offset = transform.position - player.transform.position;
    }

    void LateUpdate()
    {
        //Establece la posición del transform de la cámara para que sea la misma que la del jugador, pero offset por la distancia de offset calculada.
        transform.position = player.transform.position + offset;
    }

}
