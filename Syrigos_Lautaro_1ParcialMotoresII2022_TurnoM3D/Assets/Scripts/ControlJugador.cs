﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlJugador : MonoBehaviour
{
    //VARIABLES
    //MOVIMIENTO DEL JUGADOR.
    public float VelocidadJugador = 5.0f;//Velocidad de movimiento del jugador.
    public float VelocidadRotacion = 200.0f;//Velocidad de rotacion del jugador.
    //public Vector2 InputVector { get; private set; }
    public Rigidbody rb;
    //ANIMACION.
    private Animator animator;//Para usar el animator del personaje.
    public float x, y;//Sirven para saber si el jugador se esta moviendo, dependiendo de los ejes X e Y.
    //VIDA.
    public float Vida = 100;//Se define la vida del jugador.
    public Image barraVida;//La imagen de la barra de vida, la cual se va a vaciar y/o llenar.
    //ATAQUE.
    public bool estaGolpeando;//Indica en que momento esta atacando.
    public bool movimientoJugador;//Identifique que cuando el jugador, se mueva.
    public float ImpulsoGolpe=10f;//Impulso del golpe.

    void Start()
    {
        animator = GetComponent<Animator>();//Para usar los valores del componente Animator, que tiene el jugador.
        GestorDeAudio.instancia.ReproducirSonido("Shout at the devil");
    }

    void FixedUpdate()
    {
        if (!estaGolpeando)//Si el jugador no esta atacando puede moverse.
        {
            transform.Rotate(0, x * Time.deltaTime * VelocidadRotacion, 0);//Para que el jugador va a poder rotar.
            transform.Translate(0, 0, y * Time.deltaTime * VelocidadJugador);//Para que el jugador va a poder desplazarse.
        }

        if (movimientoJugador)//Si la variable movimientoJugador es verdadera va a pasar algo.
        {
            rb.velocity = transform.forward * ImpulsoGolpe;//Agrega velocidad al rigidbody cuando ocurra el impulso de golpe.
        }
    }

    void Update()
    {
        //PARA QUE EL JUGADOR SE MUEVA.
        y = Input.GetAxis("Horizontal");
        x = Input.GetAxis("Vertical");
        /* transform.Rotate(0, x * Time.deltaTime * VelocidadRotacion, 0);//Para que el jugador va a poder rotar.
        transform.Translate(0, 0, y * Time.deltaTime * VelocidadJugador);//Para que el jugador va a poder desplazarse. */
        //SI TOCO ESPACIO EL JUGADOR PUEDE GOLPEAR.
        if (Input.GetKeyDown(KeyCode.Space) && !estaGolpeando)
        {
            animator.SetTrigger("Golpear");
            estaGolpeando = true;
        }
        //PARA ENVIAR LA INFORMACION DE X E Y AL ANIMATOR.
        animator.SetFloat("Velocidad X", x);
        animator.SetFloat("Velocidad Y", y);
        //VIDA.
        Vida = Mathf.Clamp(Vida, 0, 100);//No va a superar el 100 de vida establecido.
        barraVida.fillAmount = Vida / 100;//Para mover la barra.
    }

    public void DetenerGolpe()
    {
        estaGolpeando = false;
        //movimientoJugador = false;
    }

    public void MovimientoJugador()
    {
        movimientoJugador = true;
    }

    public void DetenerMovimieno()
    {
        movimientoJugador = false;
    }
    /*void OnTriggerEnter(Collider collider)
    {
        if (collider.CompareTag("Brazo"))
        {
            print("Dano");
        }
    }*/

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Coleccionable") == true)
        {
            other.gameObject.SetActive(false);
        }
    }
}
