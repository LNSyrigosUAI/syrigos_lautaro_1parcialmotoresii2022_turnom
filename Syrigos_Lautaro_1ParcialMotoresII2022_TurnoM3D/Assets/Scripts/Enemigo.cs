﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemigo : MonoBehaviour
{
    //VARIABLES.
    public int Rutina;//Rutina del enemigo
    public float Cronometro;//Para el tiempo entre rutinas.
    public Animator animator;//Para usar el animator del enemigo.
    public Quaternion quaternion;//Rotacion del enemigo.
    public float Grado;//Para detectar el grado del angulo.
    public GameObject Target;//Para empiece a reconocer los targets.
    public bool Atacar;//Para que el enemigo ataque al ver al jugador.
    public RangoDelEnemigo Rango;//Referencia del script Rango del enemigo.
    public int VidaEnemigo;
    public int DañoGolpe;

    void Start()
    {
        animator = GetComponent<Animator>();//Para invocar al animator.
        Target = GameObject.Find("Player");//Para que busque al jugador.
    }

    void Update()
    {
        ComportamientoEnemigo();
    }
    
    public void ComportamientoEnemigo()//Funcionamiento del cronometro.
    {
        if (Vector3.Distance(transform.position, Target.transform.position) > 5)//Para cuando la posicion del jugador este fuera del rango del enemigo, ejecute el codigo y haga su rotina de movimiento.
        {
            animator.SetBool("Run", false);
            Cronometro += 1 * Time.deltaTime;

            if (Cronometro >= 4)
            {
                Rutina = Random.Range(0, 2);
                Cronometro = 0;//Reiniciar cronometro.
            }

            switch (Rutina)//Para las variables de la rutina.
            {
                case 0://El enemigo se queda quieto.
                    animator.SetBool("Walk", false);
                    break;
                case 1://Se determina la direccion por donde se desplaza.
                    Grado = Random.Range(0, 360);
                    quaternion = Quaternion.Euler(0, Grado, 0);
                    Rutina++;
                    break;
                case 2://La rotacion del enemigo.
                    transform.rotation = Quaternion.RotateTowards(transform.rotation, quaternion, 0.5f);
                    transform.Translate(Vector3.forward * 1 * Time.deltaTime);
                    animator.SetBool("Walk", true);
                    break;
            }
        }
        else//En caso que no, el enemigo empieza a seguir al jugador.
        {
            var LookPosition = Target.transform.position - transform.position;
            LookPosition.y = 0;
            var Rotation = Quaternion.LookRotation(LookPosition);

            if (Vector3.Distance(transform.position, Target.transform.position) > 1 && !Atacar)//Si el enemigo no esta atacando solo persigue al jugador.
            {
                //Para que rote su posicion en sentido del jugador, y asignarle una velocidad de persecucion.
                
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Rotation, 2);//El enemigo rotara segun la variable de rotacion, y se la da una velocidad(2).
                animator.SetBool("Walk", false);
                animator.SetBool("Run", true);
                transform.Translate(Vector3.forward * 2 * Time.deltaTime);//Se desplaza hacia adelante con una velocidad mayor a la de Walk.
                animator.SetBool("Atacar", false);
            }
            else//Cuando este cerca del jugador lo atacara.
            {
                transform.rotation = Quaternion.RotateTowards(transform.rotation, Rotation, 2);
                animator.SetBool("Walk", false);
                animator.SetBool("Run", false);
                //animator.SetBool("Atacar", true);
                //Atacar = true;
            }
        }
    }

    public void FinalizarAnimacion()
    {
        animator.SetBool("Atacar", false);
        Atacar = false;
        Rango.GetComponent<CapsuleCollider>().enabled = true;//El collider de Rango se active.
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Brazo")
        {
            if (animator != null)
            {
                animator.Play("Atacar");
            }

            VidaEnemigo -= DañoGolpe;
        }

        if (VidaEnemigo <= 0)
        {
            Destroy(gameObject);
        }
    }
}
